import { GlNavbar } from "@gitlab/ui";
import { mount, shallowMount } from "@vue/test-utils";
import VueComponent from "vue";
import GlLogo from "../src/components/logo.vue";
import Header from "~/components/header.vue";

describe("Header", () => {
  it("creates wrapper with the component instance", () => {
    const wrapper = mount(Header);
    expect(wrapper.vm).toBeInstanceOf(VueComponent);
  });

  it("should have header element", () => {
    const wrapper = mount(Header);

    expect(wrapper.find("header").exists()).toBeTruthy();
  });

  it("should have GlNavbar  && GlLogo components", () => {
    const wrapper = mount(Header);

    expect(wrapper.findComponent(GlNavbar).exists()).toBeTruthy();
    expect(wrapper.findComponent(GlLogo).exists()).toBeTruthy();
  });

  it("component html", () => {
    const wrapper = shallowMount(Header);

    expect(wrapper.html()).toMatchSnapshot();
  });
});

/*

---creating Vue component instance to test
finally we've got to the VTU part. So let's say we want to test the header component. To test it we need to have the test instance of the component. So here is where VTU comes in. It has special functions for creating the test instance - mount and shallowMount. Let's try them out. we'll start with mount. It will create the wrapper for the instance of component. The actual component sits under `vm` property of the wrapper. Lets check that out.


-------wrapper has some nice utilities to manipulate the rendered component. For instance it can search the rendered components has specific child components


It would be hard to explain the difference between mount and shallowMount if we did not touch the toMatchSnapshot() jest matcher. So lets try that out.

 */
