import { shallowMount } from "@vue/test-utils";
import Form, { INVALID_MESSAGE_ERROR } from "~/components/form.vue";
import { GlButton } from "@gitlab/ui";

import { submitRequest } from "~/api";

jest.mock("~/api");

const mockRequestTypes = ["type1", "type2"];

describe("Form tests", () => {
  let wrapper;

  const getInput = () => wrapper.find("input");
  const getValidationMessage = () => wrapper.find(".gl-text-red-500");
  const getSubmitButton = () => wrapper.findComponent(GlButton);
  const getForm = () => wrapper.find("form");

  test("should render a list of departments", async () => {
    wrapper = shallowMount(Form, {
      propsData: {
        requests: mockRequestTypes,
      },
    });

    const getOptions = () => wrapper.find("select").findAll("option");
    expect(getOptions()).toHaveLength(mockRequestTypes.length);

    await wrapper.setProps({ requests: [] });
    expect(getOptions()).toHaveLength(0);
  });

  describe("Invalid form", () => {
    beforeEach(() => {
      wrapper = shallowMount(Form, {
        propsData: {
          requests: mockRequestTypes,
        },
      });

      const input = getInput();
      input.setValue("abc1234");
      input.trigger("input");
    });

    test("should show validation message ", () => {
      expect(getValidationMessage().text()).toBe(INVALID_MESSAGE_ERROR);
    });

    test("should disable submit button", () => {
      expect(getSubmitButton().props("disabled")).toBe(true);
    });
  });

  describe("Valid form", () => {
    beforeAll(() => {
      wrapper = shallowMount(Form, {
        propsData: {
          requests: mockRequestTypes,
        },
      });

      const input = getInput();
      input.setValue("Just wanted to say hi");
      input.trigger("input");
    });

    test("should not show validation message ", () => {
      expect(getValidationMessage().exists()).toBe(false);
    });

    test("should disable submit button", () => {
      expect(getSubmitButton().props("disabled")).toBe(false);
    });
  });

  test("should call the api method", () => {
    const message = "Hi";
    wrapper = shallowMount(Form, {
      propsData: {
        requests: mockRequestTypes,
      },
      data() {
        return {
          message,
        };
      },
    });
    const form = getForm();
    form.trigger("submit");
    expect(submitRequest).toHaveBeenCalled();
    expect(submitRequest).toHaveBeenCalledWith(message);
  });
});
